function doScrabe(){
  
  function replaceAll(find, replace, str) {
    return str.replace(new RegExp(find, 'g'), replace);
  }

  function cleanConj(c){
    var k = 0;
    for(var i = 0; i < c.length; i++){
      if(c[i] == "<" || c[i] == ";"){
        k = i;
        break;
      }
    }

    if(k != 0){
      return c.substring(0,k);
    }
    return c;
  }
  

    $("#parsedsn div").each(function(){




    var inner = $(this).html();
    if(!(inner[0] == "<" && inner[1] == "b") && !(inner[3] == "<" && inner[4] == "b")) return;

    // stavelse replacer
    inner = replaceAll('<span class="bar"></span>', '', inner);

    var beginWord = 3;
    var endWord = inner.indexOf("</b>");
    var beginType = inner.indexOf(')">')+3;
    var endType = inner.indexOf("</abb");
    var endMeaning =  inner.lastIndexOf(")");
    var fake = inner.substring(0,endMeaning-1);
    var beginMeaning = fake.lastIndexOf('(')+1;
    
    var word = inner.substring(beginWord, endWord);
    word = replaceAll("<b>","", word);
    var type = inner.substring(beginType, endType);
    var conj = replaceAll('<span class="nobr">', "", inner);


    conj = replaceAll("</span>", "", conj);
    conj = replaceAll("</i>", "", conj);
    conj = replaceAll("</b>", "", conj);
    conj = replaceAll("<i>", "", conj);
    conj = replaceAll("<b>", "", conj);

    

    conj = conj.split(",");

    conj.splice(0, 1);
    conj.splice(3, conj.length-2);


    var conjcopy = [];
    var len = conj.length;
    

    for(var i = 0; i < len; i++){
        conj[i] = cleanConj(conj[i]); 

        if((conj[i].length <= 3*word.length) && !(conj[i] == "") && !(conj[i] == " ") ){
          calc_conj = replaceAll(" ", "", conj[i]);
          conjcopy.push(calc_conj);
        }   
    }
    
    conj = conjcopy;
    conj = conj.join("el.");
    conj = replaceAll("fx", "", conj);
    conj = conj.split("el.");
    
    var new_conj = [];
    for(var i = 0; i < conj.length; i++){
      var c = conj[i];
      var p1 = c.indexOf("(");
      var p2 = c.indexOf(")");
      if(p1 != -1){
        var before = c.substring(0,p1);
        var after = c.substring(p2+1,c.length);
        var middle = c.substring(p1+1,p2);

        new_conj.push(before + after);
        new_conj.push(before + middle + after);

      } else {
        new_conj.push(c);
      }
    }
    conj = new_conj;


    var meaning = inner.substring(beginMeaning, endMeaning);
    meaning = replaceAll('<span class="nobr">',"", meaning);
    meaning = replaceAll('</span>',"",meaning);
    meaning = replaceAll('<span class="caps">',"",meaning);
    meaning = replaceAll('<wbr>',"",meaning);

    meaning = meaning.replace('<span class="caps">LINGVISTIK</span>',"");
    meaning = meaning.replace('<abbr title="forkortelse for">',"");
    meaning = meaning.replace('</abbr>',"");

    meaning = replaceAll("<b>","", meaning);
    meaning = replaceAll("</b>","", meaning);
    meaning = replaceAll("<i>","", meaning);
    meaning = replaceAll("</i>","", meaning);
    meaning = replaceAll(" <","", meaning);
    meaning = meaning.replace("(","");
    meaning = meaning.replace(")","");
    
    if(type.length > 20){
      type = "fork.";
    }

    function success(response){
    }

    var data = {
      word: word,
      type: type,
      conj: conj,
      meaning: meaning
    }


// to save local
/*
    $.ajax({
      type: "POST",
      url: "store_scrabe.php",
      data: data,
      success: success
    });
*/

    var conjugations = conj;
    var len = conjugations.length;
    for(var j = 0; j < len; j++){
      var conj = $('<input type="text" class="conjugation" value="' + conjugations[j] + '" />');
      var del = $('<div class="delete" onclick="deleteConj(this)"></div>');

      $("#conjugations").append(conj);
      $("#conjugations").append(del);
    }

  });

}