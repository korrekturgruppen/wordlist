var url = "https://api.grammatic.al/api/v1/"
var appkey = prompt("Please enter your appkey", "Appkey");

var datas;
var item_id;

function deleteConj(item){
  $(item).prev().remove();
  $(item).remove();
}

function deleteItem(index, button){
  var data = datas[index];

  var confirm = window.confirm("You are about to delete '" + data.word + "'. Proceed?");
  if(!confirm) return;

  $(button).prev().remove();
  $(button).remove();
  

  var method = "words/delete/" + data.id;
  console.log("method " + method);
  post("POST", method, {},function(response){
      datas[index] = null;
  });
  

}

function clickItem(index){
  $("#right").show();
  $("#conjugations").empty();
  item_id = index;


  var item = datas[index];

  $("#word_id").val(item.id);
  $("#word_word").val(item.word);
  $("#word_word_type").prop("selectedIndex",item.word_type.id);
  $("#word_meaning").val(item.meaning);
  $("#word_has_other_meaning").prop('checked', item.has_other_meaning == 1);
  $("#word_origin").val(item.origin);
  for(var i = 0; i < item.conjugations.length; i++){
    var conj = $('<input type="text" class="conjugation" value="' + item.conjugations[i].value + '" />');
    var del = $('<div class="delete" onclick="deleteConj(this)"></div>');

    $("#conjugations").append(conj);
    $("#conjugations").append(del);
    
  }
  
}


function search(){
  $("#wordlist_content").empty();
  var word = $("#searchfield").val();
  var data = {
    search: word
  };
  
  function success(response){
    datas = response.data;

    for(var i = 0; i < response.data.length; i++){
      var new_entry = $('<div class="list-item" onclick="clickItem('+i+')">' + response.data[i].word + '</div><div onclick="deleteItem('+i+', this)" class="deleteItem"></div>');  
      $("#wordlist_content").append(new_entry);
    }
      
  }
  post("POST","words/search",data,success);

}



function add(){

  
  var data = {
    word: $("#addfield").val(),
    origin: "grammatic.al wordlist"
  };
  
  function success(response){
    datas = response.data;
    console.log(datas);

    item = datas[0];
    console.log(item);

    $("#right").show();
    $("#addfield").val('');
    $("#word_id").val(item.id);
    $("#word_word").val(item.word);
    $("#word_origin").val(item.origin);
  }

  post("POST","words/add",data,success);

}


function scapeDsn(){
  var word = $("#word_word").val();
  var dsn_url = 'dsn.php?q=' + word;
  


  function success(response){
    $("#parsedsn").empty();

    html = $.parseHTML( response );
    $("#parsedsn").append(html);
    doScrabe();
  }

  $.ajax({
    type: "GET",
    url: dsn_url,
    success: success
  });

}



function error(XMLHttpRequest, textStatus, errorThrown) {
   alert("Error: " + textStatus + ": " + errorThrown);
}


function post(type,method,data,success){
  console.log(type);
  console.log(method);
  console.log(data);
  console.log(success);

  $.ajax({
    type: type,
    url: url+method,
    data: data,
    headers: { 'Appkey': appkey },
    success: success,
    error: error
  });
}

function save(){

  var id = parseInt($("#word_id").val());
  var word = $("#word_word").val();
  var word_type = parseInt($("#word_word_type").prop("selectedIndex"));
  var meaning = $("#word_meaning").val();
  var has_other_meaning  = $("#word_has_other_meaning").prop('checked') ? 1 : 0;
  var origin = $("#word_origin").val();

  var conjs = $("#conjugations").children();
  var conjugations = [];
  for(var i = 0; i < conjs.length; i++){
    if(i % 2 == 1) continue;
    conjugations.push($(conjs[i]).val());
  }

  var data = {
    word: {
      id: id,
      word: word,
      word_type: word_type,
      meaning: meaning,
      has_other_meaning: has_other_meaning,
      origin: origin,
      conjugations: conjugations
    }
  }



  function success(response){
    //console.log(response);
  }
  post("POST", "words/update",data,success);



  /* this is to update the local cache data row */
  var new_data = data.word;
  var conj = [];
  for(var i = 0; i < new_data.conjugations.length; i++){
    conj.push({
      word_id: new_data.id,
      value: new_data.conjugations[i]
    })
  }
  
  new_data.word_type = {
    id: new_data.word_type
  };
  new_data.conjugations = conj;

  datas[item_id] = new_data;
}
