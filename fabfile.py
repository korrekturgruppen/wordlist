from fabric.api import local, run, sudo, env, settings, cd
from fabric.context_managers import shell_env

import re
import sys

# global names
env.repo_name = 'wordlist'
env.project_name = 'wordlist'
env.prod_parent = '/var/www/apache/'
env.git_base = '/var/git/repos/'

def prod():
    """
    Production environment
    """
    env.user = 'deploy'
    env.hosts = ['grammatic.al']
    env.deploy_name = 'grammatical-blog'
    env.prod_repo = 'production'
    # env.key_filename = '../deploy.pem'

def deploy():
    """
    Deploy to production server
    """
    prepare_deploy()
    production_dir = env.prod_parent + env.project_name
    git_bare_dir = '%s%s.git' % (env.git_base, env.project_name)

    if run('test -d %s' % git_bare_dir, warn_only=True).failed:
        run('mkdir %s' % git_bare_dir)

        with cd(git_bare_dir):
            run('git init --bare')

    add_deploy_downstream(git_bare_dir)

    # make sure we are on the master branch before we push anything
    # local('git checkout origin/master')

    local('git push %s --all' % env.prod_repo)
    # Also push tags
    local('git push %s --tags' % env.prod_repo)

    if run('test -d %s' % production_dir, warn_only=True).failed:
        with cd(env.prod_parent):
            # clone from bare repo
            run('git clone %s' % git_bare_dir)
    with cd(production_dir):
        # first checkout master so we are not dettached from a branch
        run('git checkout master')
        run('git pull --tags')
        run('git checkout %s' % env.release_tag)

    update(production_dir)

def rollback(tag=None):
    """
    Rollback to specific tag or commit
    """
    production_dir = env.prod_parent + env.project_name

    with cd(production_dir):
        # first checkout master so we are not dettached from a branch
        run('git checkout master')
        run('git pull --tags')

        # get latest tag
        result = local('git tag', capture=True)
        tags = result.split('\n')

        reg = re.compile('\^+')

        if tag == None:
            tag = tags[1] # second newest tag (one rollback)
        elif reg.match(tag):
            back = len(tag)
            if back > len(tags)-1:
                sys.stdout.write("No possible rollback tags from this point. ")
                sys.stdout.write("Try a specific commit.\n")
                exit(1)
            tag = tags[back]

        run('git checkout %s' % tag)

    # restart node server
    restart_server()

# helper functions
def prepare_deploy():
    """
    Prepare deployment by tagging current HEAD
    """
    # tag commit if not already tagged
    with settings(warn_only=True):
        result = local('git describe --exact-match HEAD', capture=True)
        if result.return_code > 0:
            # tag the commit
            import time
            release_time = time.strftime('%Y%m%d%H%M%S')
            current_commit = local('git rev-parse HEAD', capture=True)
            env.release_tag = 'deployment-%s' % release_time
            tag_msg = 'Deployment %s @ %s' % (release_time, current_commit)
            local("git tag -a %s -m '%s'" % (env.release_tag, tag_msg))
        else:
            env.release_tag = result


def add_deploy_downstream(git_dir):
    downstream = "git@%s:%s" % (env.host, git_dir)
    with settings(warn_only=True):
        if local('git remote add %s %s' % (env.prod_repo, downstream)).failed:
            local('git remote set-url %s %s' % (env.prod_repo, downstream))

def update(production_dir):
    pass
